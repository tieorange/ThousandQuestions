//
//  MainScreenView.swift
//  ThousandQuestions Watch App
//
//  Created by anduser on 19.10.2023.
//

import SwiftUI

struct MainScreenView: View {
    var body: some View {
        NavigationView{
            VStack {
                Spacer()
                
                NavigationLink(destination: CategoriesView()){
                    Text("📋 Categories")
                        .padding(.leading, 20)
                    Spacer()
                }
                
                NavigationLink(destination: CategoriesView()){
                    Text("🔄 Random")
                        .padding(.leading, 20)
                    Spacer()
                }
            }.navigationTitle("1000 questions")
        }
    }
}

#Preview {
    MainScreenView()
}
