//
//  CategoriesView.swift
//  ThousandQuestions Watch App
//
//  Created by anduser on 18.10.2023.
//

import SwiftUI

struct QuesionsCategory : Identifiable{
    let id = UUID()
    let category: String
    
    static func getDummy() -> Array<QuesionsCategory>{
        return [
            QuesionsCategory(category: "👶 Childhood"),
            QuesionsCategory(category: "🏀 Hobbies"),
            QuesionsCategory(category: "❤️ Relationships"),
            QuesionsCategory(category: "🧘‍♂️ Spirituality"),
            QuesionsCategory(category: "🙋‍♂️ Other"),
        ]
    }
}

struct CategoriesView: View {
    let categories = QuesionsCategory.getDummy()
    
    var body: some View {
        
            List(categories) { category in
                NavigationLink(destination: QuestionsView(navigationTitle: category.category)){
                    Text(category.category)
                }
            }.navigationTitle("Categories")
        
    }
}

#Preview {
    CategoriesView()
}

