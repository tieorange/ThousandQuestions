//
//  ContentView.swift
//  ThousandQuestions Watch App
//
//  Created by anduser on 04.10.2023.
//

import SwiftUI
import UIKit

struct QuestionsView: View {
    var navigationTitle: String
    @State private var index: Int = 0

    let questionsList = ["What musical album do you listen to over and over again and why?",
                         "Do you sing when you take a shower or a bath?",
                         "Which movie character do you think you could identify with the most?",
                         "What is your funniest photo?",
                         "What is the funniest letter you have ever received and from whom?",
                         "What effect does advertising have on you?",
                         "If you had your own rock band, what would you call it?",
                         "If you were to write a book, who would you dedicate it to?",
                         "What do you pay attention to first in museums?",
                         "Who do you think is the most romantic singer?",
                         "Which fairy-tale character do you associate yourself with?",
                         "What musical album do you listen to over and over again and why?",
                         "What do you usually do when you are alone?"]
    
    
    var body: some View {
        VStack {
            Text(questionsList[index])
                .fitSystemFont(lineLimit: 7)
            
            Spacer()
            
            HStack(alignment: .top) {
                LeftButton(index: $index)
                HeartButton()
                RightButton(index: $index, totalQuestions: questionsList.count)
            }
        }.navigationTitle(navigationTitle)
    }
}

struct LeftButton: View {
    @Binding var index: Int
    
    var body: some View {
        Button {
            if(index != 0){
                index -= 1
            }
        } label: {
            Text("←")
                .font(.system(size: 25))
        }
    }
}

struct HeartButton: View {
    @State private var isShowingToast = false

    var body: some View {
        Button {
            isShowingToast = true;
        } label: {
            Text("❤️")
                .font(.system(size: 25))
        }
    }
}

struct RightButton: View {
    @Binding var index: Int
    var totalQuestions: Int
    
    var body: some View {
        Button {
            if(index < totalQuestions - 1){
                index += 1
            }
        } label: {
            Text("→")
                .font(.system(size: 25))
        }
    }
}

#Preview {
    QuestionsView(navigationTitle: "Childhood")
}
