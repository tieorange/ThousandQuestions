//
//  ThousandQuestionsApp.swift
//  ThousandQuestions Watch App
//
//  Created by anduser on 04.10.2023.
//

import SwiftUI

@main
struct ThousandQuestions_Watch_AppApp: App {
    var body: some Scene {  
        WindowGroup {
            MainScreenView()
        }
    }
}
