//
//  ContentView.swift
//  ThousandQuestions Watch App
//
//  Created by anduser on 04.10.2023.
//

import SwiftUI

struct ContentView: View {
    @State private var index: Int = 0
    @State var questionsList = ["What musical album do you listen to over and over again and why?",
                                "Do you sing when you take a shower or a bath?",
                                "Which movie character do you think you could identify with the most?",
                                "What is your funniest photo?",
                                "What is the funniest letter you have ever received and from whom?",
                                "What effect does advertising have on you?",
                                "If you had your own rock band, what would you call it?",
                                "If you were to write a book, who would you dedicate it to?",
                                "What do you pay attention to first in museums?",
                                "Who do you think is the most romantic singer?",
                                "Which fairy-tale character do you associate yourself with?",
                                "What musical album do you listen to over and over again and why?",
                                "What do you usually do when you are alone?"]
    
    
    var body: some View {
        VStack {
            Text(questionsList[index])
                .padding(.bottom, 10)
                .lineLimit(100)
                .font(.system(size: 70))
                .minimumScaleFactor(0.5)
            
        }
        .padding()
        .frame(maxHeight: .infinity, alignment: .bottom)
        
        HStack(alignment: .bottom) {
            Button("<--") {
                if(index != 0){
                    index-=1
                }
            }
            .buttonStyle(.borderedProminent)
            
            Button("-->") {
                if(index < questionsList.count - 1){
                    index+=1;
                }
            }
            .buttonStyle(.borderedProminent)
            .frame(width: 200)
            
        }.frame(maxHeight: .infinity, alignment: .bottom)
            .clipShape(RoundedRectangle(cornerRadius: 5))
        
        
        
        
        Button(action: {
            //do action
        }) {
            Text("SIGN IN")
                .frame(width: 200 , height: 50, alignment: .center)
            //You need to change height & width as per your requirement
        }
        .background(Color.blue)
        .foregroundColor(Color.white)
        .cornerRadius(5)
        
        Button(action: {
        }) {
            Text("Singleplayer").font(.system(size: 20))
                .padding()
                .background(RoundedRectangle(cornerRadius: 8).fill(Color.blue))
                .frame(minWidth: 500)
        }
        .buttonStyle(PlainButtonStyle())
        
    }
}

#Preview {
    ContentView()
}

struct FitToWidth: ViewModifier {
    var fraction: CGFloat = 1.0
    func body(content: Content) -> some View {
        GeometryReader { g in
            content
                .font(.system(size: 1000))
                .minimumScaleFactor(0.005)
                .lineLimit(1)
                .frame(width: g.size.width*self.fraction)
        }
    }
}
