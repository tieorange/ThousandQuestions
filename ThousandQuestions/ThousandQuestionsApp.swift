//
//  ThousandQuestionsApp.swift
//  ThousandQuestions
//
//  Created by anduser on 04.10.2023.
//

import SwiftUI

@main
struct ThousandQuestionsApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
